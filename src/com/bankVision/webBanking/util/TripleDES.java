package com.bankVision.webBanking.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;
/**
 * 
 * @author Bankvision Software
 * @version 3.1.27 
 *
 */
public class TripleDES
{
	// ====================================================================================
	/**
	   * Use the specified TripleDES key to encrypt bytes from the input stream
	   * and write them to the output stream. This method uses CipherOutputStream
	   * to perform the encryption and write bytes at the same time.
	   */
	
	public final static String ENCRYPT_MODE				= "ENCRYPT_MODE";
	public final static String ENCRYPT_MODE_WORK_KEY	= "ENCRYPT_MODE_WORK_KEY";
	public final static String ENCRYPT_KEY_ONE			= "ENCRYPT_KEY_ONE";
	public final static String ENCRYPT_KEY_TWO			= "ENCRYPT_KEY_TWO";
	public final static String ENCRYPT_KEY_THREE		= "ENCRYPT_KEY_THREE";
	
	public static void encrypt(SecretKey key, InputStream in, OutputStream out)  
	  					throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IOException 
	{
	    // Create and initialize the encryption engine
	    Cipher cipher = Cipher.getInstance("DESede");
	    cipher.init(Cipher.ENCRYPT_MODE, key);

	    // Create a special output stream to do the work for us
	    CipherOutputStream cos = new CipherOutputStream(out, cipher);

	    // Read from the input and write to the encrypting output stream
	    byte[] buffer = new byte[2048];
	    int bytesRead;
	    while ((bytesRead = in.read(buffer)) != -1) 
	    {
	      cos.write(buffer, 0, bytesRead);
	    }
	    cos.close();

	    // For extra security, don't leave any plaintext hanging around memory.
	    java.util.Arrays.fill(buffer, (byte) 0);
	}
	// ====================================================================================
	/**
	* Use the specified TripleDES key to decrypt bytes ready from the input
	* stream and write them to the output stream. This method uses uses Cipher
	* directly to show how it can be done without CipherInputStream and
	* CipherOutputStream.
	*/
	public static void decrypt(SecretKey key, InputStream in, OutputStream out)
	      throws NoSuchAlgorithmException, InvalidKeyException, IOException,
	      IllegalBlockSizeException, NoSuchPaddingException,
	      BadPaddingException 
	{
		// Create and initialize the decryption engine
		Cipher cipher = Cipher.getInstance("DESede");
		cipher.init(Cipher.DECRYPT_MODE, key);
		
		// Read bytes, decrypt, and write them out.
		byte[] buffer = new byte[2048];
		int bytesRead;
		while ((bytesRead = in.read(buffer)) != -1) 
		{
			out.write(cipher.update(buffer, 0, bytesRead));
		}

	    // Write out the final bunch of decrypted bytes
	    out.write(cipher.doFinal());
	    out.flush();
	}
	//================================================================================================
	/**
	 * 
	 * @param plainText
	 * @param keyString
	 * @return encrypt (plainText, keyString,"")
	 */
	public static String encrypt(String plainText,  String keyString ) 
	{		
		return encrypt (plainText, keyString,"");
	}
	// ====================================================================================
	/**Realiza el encriptamiento y retorna el texto encriptado
	 * 
	 * @param plainText
	 * @param keyString
	 * @param mode
	 * @return
	 */
	public static String encrypt(String plainText, String keyString, String mode) 
	{
		return encrypt(plainText, keyString, mode, false);
	}
	// ====================================================================================
	public static String encrypt(String plainText, String keyString, String mode, boolean hexadecimalEntry) 
	{
		String encryptedText = "";
		Util util = new Util();
		byte[] keyBuffer = new byte[24]; // a Triple DES key is a byte[24] array = 3 key of 8 bytes  

		keyBuffer = util.hexStringToByteArray(keyString);
		//keyBuffer = keyString.getBytes();
		// Make the Key
		SecretKey key = new SecretKeySpec(keyBuffer, "DESede");		
		Cipher cipher ;
		byte[] plainTextBuffer;
		try
		{
			cipher = Cipher.getInstance("DESede"+mode);  // Default mode = DESede/ECB/PKCS5Padding 
			
			if(mode.contains("ECB"))
				cipher.init(Cipher.ENCRYPT_MODE, key);
			else if (mode.contains("CBC"))
				cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(new byte[8]));
			
			if(hexadecimalEntry)
				plainTextBuffer = util.hexStringToByteArray(plainText);
			else
				plainTextBuffer = plainText.getBytes();
			
			byte[] encryptedTextBuffer =  cipher.doFinal(plainTextBuffer);
			encryptedText = Util.bytesToHex(encryptedTextBuffer);// Cifrado +  hex :
		} 
		catch (Exception e)
		{
			System.err.println(e.getMessage());
		}
		return encryptedText;
	}	
	//================================================================================================
	/**Encripta los bytes
	 * 
	 * @param plainText
	 * @param keyString
	 * @param mode
	 * @return
	 */
	public static String encryptbyte(String plainText, String keyString, String mode ) 
	{
		String encryptedText = "";
		byte[] keyBuffer = new byte[24]; // a Triple DES key is a byte[24] array = 3 key of 8 bytes  

		keyBuffer = new Util().hexStringToByteArray(keyString);
		//keyBuffer = keyString.getBytes();
		// Make the Key
		SecretKey key = new SecretKeySpec(keyBuffer, "DESede");		
		Cipher cipher ;
		
		byte[] defaultBytes = plainText.getBytes();
		
		try {
			//se aplica sha1 sin convertirlo en hex
			MessageDigest algorithm = MessageDigest.getInstance("SHA-1");
			algorithm.reset();
			algorithm.update(defaultBytes);
			byte messageDigest[] =algorithm.digest();
			//se crea una variable tipo byte conte de 32 posiciones para que se llene con los 20 bytes de sha1 y el resto de posiciones en cero
			byte conte[]= new byte[32];
			for (int i = 0;i< conte.length; i++)
			{			
				if (i<messageDigest.length)conte[i]=messageDigest[i];				
			}
		//se aplica 3des a la variable tipo byte
			cipher = Cipher.getInstance("DESede"+mode);  // Default mode = DESede/ECB/PKCS5Padding 
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainTextBuffer = conte;
			byte[] encryptedTextBuffer =  cipher.doFinal(plainTextBuffer);
			encryptedText = Util.bytesToHex(encryptedTextBuffer);// Cifrado +  hex :
			//			decrypt(keyString, new String(Base64.encodeBase64(encryptedTextBuffer)),"DESede");
			
		} catch (NoSuchAlgorithmException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e)
		{
			System.err.println(e.getMessage());
		}
		return encryptedText;
	}	
	//================================================================================================
	/**
	 * 
	 * @param cryptedText
	 * @param keyString
	 * @return <code>decrypt (cryptedText,keyString,"")</code>
	 */
	public static String decrypt(String cryptedText,  String keyString) 
	{
		return decrypt (cryptedText,keyString,"");
	}
	//================================================================================================
	/**
	 * 
	 * @param encryptionParameters
	 * @param cryptedText
	 * @return <code>decrypt(cryptedText,encryptionParameters.get(ENCRYPT_KEY_ONE)+
						encryptionParameters.get(ENCRYPT_KEY_TWO)+encryptionParameters.get(ENCRYPT_KEY_THREE),
						encryptionParameters.get(ENCRYPT_MODE))</code>
	 */
	public static String decrypt400(HashMap<String,String> encryptionParameters, String cryptedText) 
	{
		return decrypt(cryptedText,encryptionParameters.get(ENCRYPT_KEY_ONE)+
						encryptionParameters.get(ENCRYPT_KEY_TWO)+encryptionParameters.get(ENCRYPT_KEY_THREE),
						encryptionParameters.get(ENCRYPT_MODE));
	}
	//================================================================================================
	/**
	 * 
	 * @param cryptedText
	 * @param keyString
	 * @param mode
	 * @return <code>decryptedText.trim()</code>
	 */
	public static synchronized String decrypt(String cryptedText,  String keyString, String mode) 
	{
		return decrypt(cryptedText, keyString, mode, false);
	}
	//================================================================================================
	public static synchronized String decrypt(String cryptedText,  String keyString, String mode, boolean hexadecimal) 
	{
		String decryptedText = "";
		byte[] keyBuffer = new byte[24]; // a Triple DES key is a byte[24] array
		// Make the Key
		keyBuffer = new Util().hexStringToByteArray(keyString);
//		keyBuffer = keyString.getBytes();
		SecretKey key = new SecretKeySpec(keyBuffer, "DESede");

		try
		{
			Cipher cipher = Cipher.getInstance("DESede"+mode);  // Default mode = DESede/ECB/PKCS5Padding 
			if(mode.contains("ECB"))
				cipher.init(Cipher.DECRYPT_MODE, key);
			else if (mode.contains("CBC"))
				cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(new byte[8]));
			
			byte[] cryptedTextBuffer = Util.hexToBytes(cryptedText);
			byte[] plainTextBuffer = cipher.doFinal(cryptedTextBuffer);
			
			if(hexadecimal)
				decryptedText = new String(Util.bytesToHex(plainTextBuffer));
			else
				decryptedText = new String(plainTextBuffer);
		}
		catch (Exception exception)
		{
			Util.log(exception.getMessage(), exception);
		}
		return decryptedText.trim();
	}
	//==========================================================================================
	/**
	 * 
	 * @param plainText
	 * @param keyString
	 * @return <code>ecryptedText</code>
	 */
	public static String encrypModestTest(String plainText, String keyString ) 
	{
		String ecryptedText = "1";
		byte[] keyBuffer = new byte[24]; // a Triple DES key is a byte[24] array = 3 key of 8 bytes

		keyBuffer = new Util().hexStringToByteArray(keyString);
//		for (int i = 0; i < keyString.length() && i < keyBuffer.length; i++) 
//			keyBuffer[i] = keyString.charAt(i);
		// Make the Key
		SecretKey key = new SecretKeySpec(keyBuffer, "DESede");		
		Cipher cipher ;
		try
		{
			System.out.println("DESede:");
			cipher = Cipher.getInstance("DESede");  // Default mode = DESede/ECB/PKCS5Padding
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainTextBuffer = plainText.getBytes();
			byte[] encryptedTextBuffer =  cipher.doFinal(plainTextBuffer);
			System.out.println("Texto origen:" + new String(plainTextBuffer));
			System.out.println("Texto cifrado:" + new String(encryptedTextBuffer));
			System.out.println("Cifrado +  Base 64:" + new String(Base64.encodeBase64(encryptedTextBuffer)));			
			System.out.println("------------------------");
			decrypt(keyString, new String(Base64.encodeBase64(encryptedTextBuffer)),"DESede");
			System.out.println("==========================================");
		} catch (Exception e)
		{
			System.err.println(e.getMessage());
		}
		try
		{
			System.out.println("DESede/CBC/NoPadding");
			cipher = Cipher.getInstance("DESede/CBC/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainTextBuffer = plainText.getBytes();
			byte[] encryptedTextBuffer =  cipher.doFinal(plainTextBuffer);
			System.out.println("Texto origen:" + new String(plainTextBuffer));
			System.out.println("Texto cifrado:" + new String(encryptedTextBuffer));
			System.out.println("Cifrado +  Base 64:" + new String(Base64.encodeBase64(encryptedTextBuffer)));			
			System.out.println("------------------------");
			decrypt(keyString, new String(Base64.encodeBase64(encryptedTextBuffer)),"DESede/CBC/NoPadding:");
		} catch (Exception e)
		{
			System.err.println(e.getMessage());
		}
		System.out.println("==========================================");
		try
		{
			System.out.println("DESede/CBC/PKCS5Padding");
			cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainTextBuffer = plainText.getBytes();
			byte[] encryptedTextBuffer =  cipher.doFinal(plainTextBuffer);
			System.out.println("Texto origen:" + new String(plainTextBuffer));
			System.out.println("Texto cifrado:" + new String(encryptedTextBuffer));
			System.out.println("Cifrado +  Base 64:" + new String(Base64.encodeBase64(encryptedTextBuffer)));			
			System.out.println("------------------------");
			decrypt(keyString, new String(Base64.encodeBase64(encryptedTextBuffer)),"DESede/CBC/NoPadding");
		} catch (Exception e)
		{
			System.err.println(e.getMessage());
		}
		System.out.println("==========================================");
		try
		{
			System.out.println("DESede/ECB/NoPadding");
			cipher = Cipher.getInstance("DESede/ECB/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainTextBuffer = plainText.getBytes();
			byte[] encryptedTextBuffer =  cipher.doFinal(plainTextBuffer);
			System.out.println("Texto origen:" + new String(plainTextBuffer));
			System.out.println("Texto cifrado:" + new String(encryptedTextBuffer));
			System.out.println("Cifrado +  Base 64:" + new String(Base64.encodeBase64(encryptedTextBuffer)));			
			System.out.println("------------------------");
			decrypt(keyString, new String(Base64.encodeBase64(encryptedTextBuffer)),"DESede/ECB/NoPadding");
		} catch (Exception e)
		{
			System.err.println(e.getMessage());
		}
		System.out.println("==========================================");
		try
		{
			System.out.println("DESede/ECB/PKCS5Padding");
			cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainTextBuffer = plainText.getBytes();
			byte[] encryptedTextBuffer =  cipher.doFinal(plainTextBuffer);
			System.out.println("Texto origen:" + new String(plainTextBuffer));
			System.out.println("Texto cifrado:" + new String(encryptedTextBuffer));
			System.out.println("Cifrado +  Base 64:" + new String(Base64.encodeBase64(encryptedTextBuffer)));
			System.out.println("------------------------");
			decrypt(keyString, new String(Base64.encodeBase64(encryptedTextBuffer)),"DESede/ECB/PKCS5Padding");
		} catch (Exception e)
		{
			System.err.println(e.getMessage());
		}
		System.out.println("==========================================");
		try
		{
			System.out.println("DESede/CFB/NoPadding");
			cipher = Cipher.getInstance("DESede/CFB/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainTextBuffer = plainText.getBytes();
			byte[] encryptedTextBuffer =  cipher.doFinal(plainTextBuffer);
			System.out.println("Texto origen:" + new String(plainTextBuffer));
			System.out.println("Texto cifrado:" + new String(encryptedTextBuffer));
			System.out.println("Cifrado +  Base 64:" + new String(Base64.encodeBase64(encryptedTextBuffer)));
			System.out.println("------------------------");
			decrypt(keyString, new String(Base64.encodeBase64(encryptedTextBuffer)),"DESede/CFB/NoPadding");
		} catch (Exception e)
		{
			System.err.println(e.getMessage());
		}
		System.out.println("==========================================");
		try
		{
			System.out.println("DESede/CFB/PKCS5Padding");
			cipher = Cipher.getInstance("DESede/CFB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainTextBuffer = plainText.getBytes();
			byte[] encryptedTextBuffer =  cipher.doFinal(plainTextBuffer);
			System.out.println("Texto origen:" + new String(plainTextBuffer));
			System.out.println("Texto cifrado:" + new String(encryptedTextBuffer));
			System.out.println("Cifrado +  Base 64:" + new String(Base64.encodeBase64(encryptedTextBuffer)));			
			System.out.println("------------------------");
			decrypt(keyString, new String(Base64.encodeBase64(encryptedTextBuffer)),"DESede/CFB/PKCS5Padding");
		} catch (Exception e)
		{
			System.err.println(e.getMessage());
		}
		System.out.println("==========================================");
		return ecryptedText;
	}
	//==========================================================================================
}
