package com.bankVision.webBanking.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
/**Manage status
 * 
 * @author Bankvision Software
 * @version 3.1.27
 *
 */
public class Status
{
	//====================================================================================
	/**
	 * The lis tof Exception objects.
	 */
	private List<Exception> exceptions;

	public Status() {
		exceptions = new ArrayList<Exception>();
	}
	//====================================================================================
	/**
	 * 
	 * @return (exceptions.size() == 0)
	 */
	public boolean isSuccessful()
	{
		return (exceptions.size() == 0);
	}
	//====================================================================================
	/**<code>exceptions.add(ex)</code>
	 * 
	 * @param ex
	 */
	public void addException(Exception ex)
	{
		exceptions.add(ex);
	}
	//====================================================================================
	/**
	 * 
	 * @return exceptions.iterator()
	 */
	public Iterator<Exception> getExceptions()
	{
		return exceptions.iterator();
	}
	//====================================================================================
}
