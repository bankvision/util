package com.bankVision.webBanking.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**Contiene el encriptamiento y desentriptamiento de transaccionalserver para las transacciones realizadas
 * 
 * @author Bankvision Software
 * @version 3.1.27
 *
 */
public class DES
{
	// ====================================================================================
	static private final String DES 				= "DES";
	static private final String NO_PADDING 			= "NoPadding";
	static private final String ZERO_BYTE_PADDING	= "ZeroBytePadding";
	// ====================================================================================
	/**
	 * Use the specified DES key to encrypt bytes from the input stream
	 * and write them to the output stream. This method uses CipherOutputStream
	 * to perform the encryption and write bytes at the same time.
	 * 
	 * @param key
	 * @param in
	 * @param out
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws NoSuchPaddingException
	 * @throws IOException
	 */
	public static void encrypt(SecretKey key, InputStream in, OutputStream out)  
	  					throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IOException 
	{
	    // Create and initialize the encryption engine
	    Cipher cipher = Cipher.getInstance(DES);
	    cipher.init(Cipher.ENCRYPT_MODE, key);

	    // Create a special output stream to do the work for us
	    CipherOutputStream cos = new CipherOutputStream(out, cipher);

	    // Read from the input and write to the encrypting output stream
	    byte[] buffer = new byte[2048];
	    int bytesRead;
	    while ((bytesRead = in.read(buffer)) != -1) 
	    {
	      cos.write(buffer, 0, bytesRead);
	    }
	    cos.close();

	    // For extra security, don't leave any plaintext hanging around memory.
	    java.util.Arrays.fill(buffer, (byte) 0);
	}
	// ====================================================================================
	/**
	* Use the specified DES key to decrypt bytes ready from the input
	* stream and write them to the output stream. This method uses uses Cipher
	* directly to show how it can be done without CipherInputStream and
	* CipherOutputStream.
	* 
	* @param key
	* @param in
	* @param out
	* @throws NoSuchAlgorithmException
	* @throws InvalidKeyException
	* @throws IOException
	* @throws IllegalBlockSizeException
	* @throws NoSuchPaddingException
	* @throws BadPaddingException
	*/
	public static void decrypt(SecretKey key, InputStream in, OutputStream out)
	      throws NoSuchAlgorithmException, InvalidKeyException, IOException, IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException 
	{
		// Create and initialize the decryption engine
		Cipher cipher = Cipher.getInstance(DES);
		cipher.init(Cipher.DECRYPT_MODE, key);
		
		// Read bytes, decrypt, and write them out.
		byte[] buffer = new byte[2048];
		int bytesRead;
		while ((bytesRead = in.read(buffer)) != -1) 
		{
			out.write(cipher.update(buffer, 0, bytesRead));
		}

	    // Write out the final bunch of decrypted bytes
	    out.write(cipher.doFinal());
	    out.flush();
	}
	// ====================================================================================
	/**a DES key is a array of 8 bytes<p>
	 * Make the Key<p>
	 * Data is encrypted with random key<p>
	 * 
	 * 
	 * @param plainText
	 * @param keyString
	 * @param mode
	 * @return encryptedText
	 */
	public static String encrypt(String plainText, String keyString, String mode)
	{
		String encryptedText = "";
		byte[] keyBuffer = new byte[8]; // a DES key is a array of 8 bytes    
		keyBuffer = new Util().hexStringToByteArray(keyString);
		
		// Make the Key
		SecretKey key = new SecretKeySpec(keyBuffer, DES);
		Cipher cipher;
		try
		{
			/**
			 *  Data is encrypted with random key
			 */
			byte[] plainTextBuffer = { };
			if (mode.contains(ZERO_BYTE_PADDING))
			{
				mode = mode.replace(ZERO_BYTE_PADDING, NO_PADDING);
			}
			cipher = Cipher.getInstance(DES + mode);
			plainTextBuffer = Util.zeroBytePadding(Util.hexToBytes(plainText));
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] encryptedTextBuffer =  cipher.doFinal(plainTextBuffer);
			encryptedText = Util.bytesToHex(encryptedTextBuffer);
		} 
		catch (Exception e)
		{
			Util.log(e.getMessage());
		}
		return encryptedText;
	}
	//================================================================================================
	/**Obtain the parameters for return it in string
	 * 
	 * @param cryptedText
	 * @param keyString
	 * @param mode
	 * @return decrypt(cryptedText, keyString, mode, false)
	 */
	public static String decrypt(String cryptedText, String keyString, String mode)
	{
		return decrypt(cryptedText, keyString, mode, false);
	}
	//================================================================================================
	/**a DES key is a byte[8] array<p>
	 * Make the Key<p>
	 * 
	 * 
	 * @param cryptedText
	 * @param keyString
	 * @param mode
	 * @param hexadecimal
	 * @return decryptedText.trim()
	 */
	public static String decrypt(String cryptedText, String keyString, String mode, boolean hexadecimal) 
	{
		String decryptedText = "";
		byte[] keyBuffer = new byte[8]; // a DES key is a byte[8] array
		// Make the Key
		keyBuffer = new Util().hexStringToByteArray(keyString);
		SecretKey key = new SecretKeySpec(keyBuffer, "DES");

		try
		{
			if (mode.contains(ZERO_BYTE_PADDING))
				mode = mode.replace(ZERO_BYTE_PADDING, NO_PADDING);
			Cipher decipher = Cipher.getInstance("DES" + mode);  // Default mode = DES/ECB/NoPadding 
			decipher.init(Cipher.DECRYPT_MODE, key);
			byte[] cryptedTextBuffer = Util.hexToBytes(cryptedText);
			byte[] plainTextBuffer = decipher.doFinal(cryptedTextBuffer);
			
			if(hexadecimal)
				decryptedText = new String(Util.bytesToHex(plainTextBuffer));
			else
				decryptedText = new String(plainTextBuffer);
		} 
		catch (Exception exception)
		{
			Util.log(exception.getMessage(), exception);
		}
		return decryptedText.trim();
	}
	//================================================================================================
	/**Make the private Key<p>
	 * Decryption of the data encryption key is performed using the private key<p>
	 * The size of data is obtained subtracting the size of the encrypted key<p>
	 * Decryption of data is performed using the key obtained in the previous step<p>
	 * 
	 * 
	 * @param cryptedText
	 * @param keyString
	 * @param mode
	 * @return decryptedText.trim()
	 */
	public static synchronized String decryptWithEncryptedKey(String cryptedText, String keyString, String mode)
	{
		String decryptedText = "";

		/**
		 *  Make the private Key
		 */
		byte[] keyBuffer = new Util().hexStringToByteArray(keyString);		
		SecretKey key = new SecretKeySpec(keyBuffer, DES);
		
		try
		{
			if (mode.contains(ZERO_BYTE_PADDING))
				mode = mode.replace(ZERO_BYTE_PADDING, NO_PADDING);
				
			Cipher decipher = Cipher.getInstance(DES + mode);
			decipher.init(Cipher.DECRYPT_MODE, key);

			byte[] cryptedTextBuffer = Util.hexToBytes(cryptedText);
			 
			byte[] cryptedKeyBuffer = new byte[8];
			int index = 0;
			for (int i = 11; i < 19; i++)
			{
				cryptedKeyBuffer[index] = cryptedTextBuffer[i];
				index++;
			}
			/**
			 *  Decryption of the data encryption key is performed using the private key 
			 */
			byte[] plainTextBuffer = decipher.doFinal(cryptedKeyBuffer);
			
			key = new SecretKeySpec(plainTextBuffer, DES);
			decipher.init(Cipher.DECRYPT_MODE, key);
			/**
			 *  The size of data is obtained subtracting the size of the encrypted key
			 */
			byte[] byteArraySize = {cryptedTextBuffer[3], cryptedTextBuffer[4], cryptedTextBuffer[5], cryptedTextBuffer[6]};
			int encryptedDataSize = Util.byteArrayToNumericValue(byteArraySize) - 8;
			byte[] cryptedDataBuffer = new byte[encryptedDataSize];
			index = 0;
			for (int i = 19; i < 19 + encryptedDataSize; i++)
			{
				cryptedDataBuffer[index] = cryptedTextBuffer[i];
				index++;
			}
			/**
			 *  Decryption of data is performed using the key obtained in the previous step 
			 */
			plainTextBuffer = decipher.doFinal(cryptedDataBuffer);
			decryptedText = new String(plainTextBuffer);
		} 
		catch (Exception exception)
		{
			Util.log(exception.getMessage(), exception);
		}
		return decryptedText.trim();
	}
	//==========================================================================================
}