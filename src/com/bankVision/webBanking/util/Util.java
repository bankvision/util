package com.bankVision.webBanking.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
/**
 * 
 * @author Bankvision Software
 * @version 3.1.27
 *
 */
public class Util 
{
	//=========================================================================================
	static Logger logger = Logger.getLogger(Util.class);
	//=========================================================================================
	public final static String SIMPLE_DATE_FORMAT 			= "yyyy/MM/dd";
	public final static String NO_SEPARATOR_DATE_FORMAT 	= "yyyyMMdd";
	public final static String SIMPLE_DATE_TIME_FORMAT 		= "yyyy/MM/dd HH:mm:ss.SSS";
	public final static String SIMPLE_TIME_FORMAT 			= "HH:mm:ss";
	public final static String ELEMENT_ATRIBUTES			= "ROW";
	public final static String ZERO_WIDTH_SPACE				= "ABCDEFGHIJKLMNOPQ";
	public final static String HEAD_XML_VERSION	 			= "HEAD_XML_VERSION";
	public final static String HEAD_XML_ENCODING	 		= "HEAD_XML_ENCODING";
	public final static String HEAD_XML_STANDALONE	 		= "HEAD_XML_STANDALONE";
	public final static String HEAD_OMIT_XML_DECLARATION	= "HEAD_OMIT_XML_DECLARATION";
	//=========================================================================================
	public final static DateFormat dateFormat 				= new SimpleDateFormat(SIMPLE_DATE_FORMAT); 
	public final static DateFormat noSeparatorDateFormat 	= new SimpleDateFormat(NO_SEPARATOR_DATE_FORMAT); 
	public final static DateFormat dateTimeFormat 			= new SimpleDateFormat(SIMPLE_DATE_TIME_FORMAT); 
	//=========================================================================================
	public Util()
	{
	}
	//=========================================================================================
	/**
	 * 
	 * @param date
	 * @return <code>  dateFormat.parse(date)</code> <code>  </code> <code>  </code>
	 * @throws ParseException
	 */
	public static synchronized Date getDate(String date) throws ParseException
	{
        return dateFormat.parse(date);
	}
	//=========================================================================================
	/**
	 * 
	 * @param date
	 * @return <code> dateFormat.format(date) </code> <code>  </code>
	 */
	public static String getDate(Date date)
	{
        return dateFormat.format(date);
	}
	//=========================================================================================	
	/**
	 * 
	 * @return <code> dateFormat.format(new Date()) </code> <code>  </code>
	 */
	public static String getDate() 
	{
        return dateFormat.format(new Date());
    }
	//=========================================================================================	
	/**
	 * 
	 * @param date
	 * @return <code> noSeparatorDateFormat.format(dateFormat.parse(date)) </code> <code>  </code>
	 * @throws ParseException
	 */
	public synchronized static String convertDateToNumber(String date) throws ParseException 
	{
		return noSeparatorDateFormat.format(dateFormat.parse(date));
    }
	//=========================================================================================	
	/**
	 * 
	 * @param date
	 * @return <code>noSeparatorDateFormat.parse(date)  </code> <code>  </code>
	 * @throws ParseException
	 */
	public synchronized static Date getDateNumberFormat(String date) throws ParseException 
	{
		return noSeparatorDateFormat.parse(date);
    }
	//=========================================================================================	
	/**
	 * 
	 * @param date
	 * @return <code> dateTimeFormat.parse(date) </code> <code>  </code>
	 * @throws ParseException
	 */
	public static Date getDateTime(String date) throws ParseException
	{
        return dateTimeFormat.parse(date);
	}
	//=========================================================================================
	/**
	 * 
	 * @param date
	 * @param customFormat
	 * @return <code>customDateTimeFormat.parse(date)  </code> <code>  </code>
	 * @throws ParseException
	 */
	public static Date getDateTime(String date,String customFormat) throws ParseException
	{
        DateFormat customDateTimeFormat = new SimpleDateFormat(customFormat);
		return customDateTimeFormat.parse(date);  
	}
	//=========================================================================================
	/**
	 * 
	 * @param date
	 * @param customFormat
	 * @return <code> customDateTimeFormat.format(date) </code> <code>  </code>
	 */
	public static String getDateTime(Date date,String customFormat)
	{
    	DateFormat customDateTimeFormat = new SimpleDateFormat(customFormat);
		return customDateTimeFormat.format(date);
	}
	//=========================================================================================
	/**
	 * 
	 * @param date
	 * @return <code> dateTimeFormat.format(date) </code> <code>  </code>
	 */
	public static String getDateTime(Date date)
	{
        return dateTimeFormat.format(date);
	}
	//=========================================================================================	
	/**
	 * 
	 * @return <code> dateTimeFormat.format(new Date()) </code> <code>  </code>
	 */
	public static String getDateTime() 
	{
        return dateTimeFormat.format(new Date());
    }
	// ====================================================================================
	public static String getTime(Date time)
	{
    	DateFormat customTimeFormat = new SimpleDateFormat(SIMPLE_TIME_FORMAT);
		return customTimeFormat.format(time);
	}
	//=========================================================================================
	/**
	 * 
	 * @param s
	 * @return <code> data </code> <code>  </code>
	 */
	public byte[] hexStringToByteArray(String s) 
	{
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) 
	    {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
	//=========================================================================================	
	/**
	 * 
	 * @param hexString
	 * @return <code>bytes  </code> <code>  </code>
	 */
	public static byte[] hexToBytes(String hexString)
	{
		HexBinaryAdapter adapter = new HexBinaryAdapter();
		byte[] bytes = adapter.unmarshal(hexString);
		return bytes;
	}
	//=========================================================================================	
	/**
	 * 
	 * @param bytes
	 * @return <code> adapter.marshal(bytes) </code> <code>  </code>
	 */
	public static synchronized String bytesToHex(byte[] bytes)
	{
		HexBinaryAdapter adapter = new HexBinaryAdapter();
		return  adapter.marshal(bytes);
	}
	//=========================================================================================
	public String stringToHex(String message) 
	{
		StringBuilder buf = new StringBuilder();
		for (char ch: message.toCharArray()) 
		{
			if (buf.length() > 0)
				buf.append("");
			
			buf.append(String.format("%01x", (int) ch));
		}
		return buf.toString();
	}
	//=========================================================================================
	/**
	 * 
	 * @param arg
	 * @return <code> arg </code> <code>  </code>
	 */
	public static String stringToHex_US_ASCII(String arg) 
	{
	    try 
	    {
			return String.format("%040x", new BigInteger(arg.getBytes("US-ASCII")));
		} 
	    catch (UnsupportedEncodingException e) 
	    {
			e.printStackTrace();
		}
		return arg;
	}
	//=========================================================================================
	/**
	 * 
	 * @param byteArray
	 * @return <code> (int)value </code> <code>  </code>
	 */
	public static int byteArrayToNumericValue(byte[] byteArray)
	{
		long value = 0;
		for (int i = 0; i < byteArray.length; i++)
		{
		   value += ((long) byteArray[i] & 0xffL) << (8 * i);
		}
		return (int)value;
	}
	//=========================================================================================
	/**
	 * 
	 * @param value
	 * @param arrayLenght
	 * @return <code> ByteBuffer.allocate(arrayLenght).order(ByteOrder.LITTLE_ENDIAN).putInt(value).array() </code> <code>  </code>
	 */
	public static byte[] intValueToByteArray(int value, int arrayLenght)
	{		
		return ByteBuffer.allocate(arrayLenght).order(ByteOrder.LITTLE_ENDIAN).putInt(value).array();
	}
	//=========================================================================================
	/**
	 * 
	 * @param byteArray
	 * @return <code>byteArray  </code> <code>  </code>
	 */
	public static byte[] zeroBytePadding(byte[] byteArray)
	{
		byte[] auxTextBuffer = { };
		int padding = byteArray.length % 8;
		if (padding > 0)
		{
			int cryptedTextBufferPadding = byteArray.length + 8 - padding;
			auxTextBuffer = new byte[cryptedTextBufferPadding];
			for (int i = 0; i < cryptedTextBufferPadding; i++)
			{
				if (i >= byteArray.length)
					auxTextBuffer[i] = (byte)0x00;
				else
					auxTextBuffer[i] = byteArray[i];
			}
			return auxTextBuffer;
		}
		else
			return byteArray;
	}
	//=========================================================================================
	/**
	 * 
	 * @param arraySize
	 * @return <code> byteArray </code> <code>  </code>
	 */
	public static byte[] getRandomByteArray(int arraySize)
	{
		byte[] byteArray = new byte[arraySize];
		new Random().nextBytes(byteArray);
		return byteArray;
	}
	//=========================================================================================
	/**Escribe en el log el mensaje
	 * 
	 * @param message
	 */
	public static synchronized void log(String message) 
	{
		if (message != null)
			logger.info(" " + message.replaceAll("\n", ""));
    }
	//=========================================================================================		
	/**Escribe el error en el log
	 * 
	 * @param message
	 * @param anException
	 */
	public static synchronized void log(String message, Exception anException) 
	{
		StringWriter stringWriter= new StringWriter();
		
		if (message != null)
			logger.error(" " +  (message + anException.getMessage()).replaceAll("\n", " "));
		
		anException.printStackTrace(new PrintWriter(stringWriter));
		logger.trace(new Date() + stringWriter.toString()); 
    }
	//=========================================================================================		
	/**
	 * 
	 * @param count
	 * @param filler
	 * @param value
	 * @param paddingLeft
	 * @return <code> value </code> <code>  </code>
	 */
	public static synchronized String paddingText(long count, String filler, String value, boolean paddingLeft) 
	{
		StringBuffer aStringBuf = new StringBuffer();
	    for (int i=0; i<count-value.length(); i++)
	        aStringBuf.append(filler);
	    if (paddingLeft)
	    	value = aStringBuf.toString() + value;
	    else
	    	value = value + aStringBuf.toString();
	    return value;
	}
	//=========================================================================================
	/**
	 * 
	 * @param id
	 * @return <code>digito  </code> <code>  </code>
	 */
	public static Integer getdigitoVerificacionNumero(String id) 
	{
		int aux = 0;
		for(int i = id.length()-1  ; i >=  0; i--)
		{
			int rs1 = 0;
			int resultado = 0;
			if (i % 2 == 0)
			{
				rs1= ((Integer.parseInt(id.substring(i,i+1)))*2); 
			}
			else
			{
				rs1= ((Integer.parseInt(id.substring(i,i+1)))*1);
			}
			if (rs1 >9)
			{
				while (rs1 > 0)
			    {
				      resultado += rs1 % 10;
			          rs1 = rs1 / 10;
			    }
				aux += resultado;
			}
			else 
				aux += rs1;
		}
		
		int digito = 0;
		int residuo = aux % 10;
		 
		if (residuo != 0)
		{
			digito = 10 - residuo;
		}
		else
			digito = residuo;
		
		return digito;
	}
	//=========================================================================================
	/**
	 * 
	 * @param filePath
	 * @param labelName
	 * @return <code> configurationXML </code> <code>  </code>
	 * @throws Exception
	 */
	public static Document loadXMLFile(String filePath) throws Exception 
	{
		DocumentBuilder aDocumentBuilder;
		DocumentBuilderFactory aDocumentBuilderFactory 	= DocumentBuilderFactory.newInstance();
		aDocumentBuilder 								= aDocumentBuilderFactory.newDocumentBuilder();
		Document document								= aDocumentBuilder.parse(filePath);
		document.getDocumentElement().normalize();

		return document;
    }
	//=========================================================================================
	public static void saveXMLFile(Document document, String filePath) throws Exception 
	{
		DOMSource source= new DOMSource(document);
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		StreamResult result = new StreamResult(filePath);
		transformer.transform(source, result);
    }
	//=========================================================================================
	/**
	 * 
	 * @param className
	 * @param objectId
	 */
	static public void releaseObjectFromCache(String className, String objectId)
    {
		if (!className.equals("--None--"))
		{
			Class<?> entityClass;
			try {
				/**
				 * Construct correctly the name of the class
				 */
				className = className.replace("[", "").replace("]", "");
				char[] aux = className.toCharArray();
				aux[0] = Character.toUpperCase(aux[0]);
				className = new String(aux);
				className = "com.bankVision.webBanking.domain." + className;
				/**
				 * Gets the object class
				 */
				entityClass = Class.forName(className);
				/**
				 * Invokes the method in order to release the object from cache
				 */
				Method method = entityClass.getMethod("release", String.class);
				method.invoke(null, objectId);
			} 
			catch (ClassNotFoundException cnfe) {
				Util.log(cnfe.getMessage());
			}
			catch (SecurityException se) {
				Util.log(se.getMessage());
			} 
			catch (NoSuchMethodException nsme) {
				Util.log(nsme.getMessage());
			}
			catch (IllegalArgumentException iaex) {
				Util.log(iaex.getMessage());
			} 
			catch (IllegalAccessException iaex) {
				Util.log(iaex.getMessage());
			} 
			catch (InvocationTargetException itex) {
				Util.log(itex.getMessage());
			}
		}
    }
	//=========================================================================================
	/**Se crea el tag que identifica los parametros y luego se crean los nodos recorriendo el hashmap<p>
	 * Se agrega el Tag al documento
	 * 
	 * @param parametersResponse
	 * @param rootTagName
	 * @return <code> document </code> <code>  </code>
	 * @throws ParserConfigurationException
	 */
	public static Document transformHashMapToDocumentXML(HashMap<String,String> parametersResponse, String rootTagName) throws ParserConfigurationException
	{
		DocumentBuilderFactory aDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder aDocumentBuilder;
		aDocumentBuilder = aDocumentBuilderFactory.newDocumentBuilder();
		Document document = aDocumentBuilder.newDocument();
		
		// Se crea el tag que identifica los parametros y luego se crean los nodos recorriendo el hashmap 
		Element xmlRootElement = document.createElement(rootTagName);
		Iterator<String> iteratorparametersResponse = parametersResponse.keySet().iterator();
		while(iteratorparametersResponse.hasNext())
		{
			String parameter = iteratorparametersResponse.next();
			Element aElement = document.createElement(parameter);
			aElement.setTextContent(parametersResponse.get(parameter));
			xmlRootElement.appendChild(aElement);
			//xmlRootElement.appendChild(document.createElement(parameter).appendChild(document.createTextNode(parametersResponse.get(parameter))));  
			//xmlRootElement.setAttribute(parameter, parametersResponse.get(parameter));
		}
		
		// Se agrega el Tag al documento
		document.appendChild(xmlRootElement);
		
		return document;
	}
	//=========================================================================================
	/**Se crea el tag que identifica los parametros y luego se crean los nodos recorriendo el hashmap<p>
	 * Se agrega el Tag al documento
	 * @param parametersResponse
	 * @param rootTagName
	 * @param listTagFieldsConstant
	 * @return <code>document  </code> <code>  </code>
	 * @throws ParserConfigurationException
	 */
	public static Document transformHashMapToDocumentXMLLoadAttributes(HashMap<String,String> parametersResponse,  String rootTagName, ArrayList<String> listTagFieldsConstant) throws ParserConfigurationException
	{
		DocumentBuilderFactory aDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder aDocumentBuilder;
		aDocumentBuilder = aDocumentBuilderFactory.newDocumentBuilder();
		Document document = aDocumentBuilder.newDocument();

		// Se crea el tag que identifica los parametros y luego se crean los nodos recorriendo el hashmap 
		Element xmlRootElement = document.createElement(rootTagName);
		OutputFormat format = new OutputFormat(document);
		format.setOmitXMLDeclaration(true);
		format.setOmitDocumentType(true);
		Element bElement = null;
		for (int index = 0;index < listTagFieldsConstant.size(); index++)
		{
			String parameter = listTagFieldsConstant.get(index);
			bElement = document.createElement(parameter);
			bElement.appendChild(document.createTextNode(ZERO_WIDTH_SPACE));
			xmlRootElement.appendChild(bElement);
		}
		Element cElement= document.createElement(ELEMENT_ATRIBUTES);
		
		Iterator<String> iteratorparametersResponse = parametersResponse.keySet().iterator();
		while(iteratorparametersResponse.hasNext())
		{
			String parameter = iteratorparametersResponse.next().replace(" ","_");
			//	Element aElement = document.createElement(parameter);
			cElement.setAttribute(parameter, parametersResponse.get(parameter));  
			//xmlRootElement.appendChild(document.createElement(parameter).appendChild(document.createTextNode(parametersResponse.get(parameter))));  
			//xmlRootElement.setAttribute(parameter, parametersResponse.get(parameter));
		}
		bElement.appendChild(cElement);
		xmlRootElement.appendChild(bElement);
		// Se agrega el Tag al documento
		document.appendChild(xmlRootElement);
		
		return document;
	}
	//=========================================================================================
	/**Transforma el documento XML en String
	 * 
	 * @param documentXML
	 * @param version
	 * @param encoding
	 * @param standalone
	 * @param omitXMLDeclaration
	 * @return <code> response </code> <code>  </code>
	 * @throws TransformerException
	 */
	public static String transformDocumentXMLToString(Document documentXML, String version, String encoding, Boolean standalone, Boolean omitXMLDeclaration) throws TransformerException 
    {
    	String response = null;
    	
    	// Transforma el documento XML en String 
    	DOMSource domSource = new DOMSource(documentXML);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        
        if(omitXMLDeclaration)
        {
        	transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        }
        else
        {
        	transformer.setOutputProperty(OutputKeys.VERSION, version);
        	transformer.setOutputProperty(OutputKeys.STANDALONE, standalone.toString());
        }
        
        transformer.setOutputProperty(OutputKeys.ENCODING, encoding);
    	
        StringWriter stringWriter = new StringWriter();
        transformer.transform(domSource, new StreamResult(stringWriter));
        response = stringWriter.toString();
        
		return response;
	}
	//=========================================================================================
	/**Se convierte una cadena xml en un Document xml<p>
	 * 
	 * 
	 * @param data
	 * @return <code> document </code> <code>  </code>
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public Document transformStringXMLToDocument(String data) throws ParserConfigurationException, SAXException, IOException
    {
    	DocumentBuilderFactory aDocumentBuilderFactory 	= DocumentBuilderFactory.newInstance();
		DocumentBuilder aDocumentBuilder				= aDocumentBuilderFactory.newDocumentBuilder();
		return aDocumentBuilder.parse(new InputSource(new StringReader(data)));
    }
	//=========================================================================================
	/**Se leen los nodos existentes pata el TAG que contiene los parametros de la transacción<p>
	 * 
	 * 
	 * @param data
	 * @param rootTagName
	 * @return <code> dataTransaction </code> <code>  </code>
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public HashMap<String, String> transformDocumentXMLToHashMap(String data, String rootTagName) throws ParserConfigurationException, SAXException, IOException
    {
    	HashMap<String, String> dataTransaction = new HashMap<String, String>();
    	
    	DocumentBuilderFactory aDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder aDocumentBuilder;
		aDocumentBuilder = aDocumentBuilderFactory.newDocumentBuilder();
		Document doc = aDocumentBuilder.parse(new InputSource(new StringReader(data)));
		doc.getDocumentElement().normalize();
		
		// Se leen los nodos existentes pata el TAG que contiene los parametros de la transacción
		Element xmlRootElement = (Element)doc.getElementsByTagName(rootTagName).item(0);
		NodeList nodeList = xmlRootElement.getChildNodes();
		for (int contNode = 0; contNode < nodeList.getLength(); contNode++) 
		{
			Node aNode = nodeList.item(contNode);
			if(aNode.getChildNodes().getLength() >1)
			{
				NodeList bnodeList = aNode.getChildNodes();
				for (int bcontNode = 0; bcontNode < bnodeList.getLength(); bcontNode++)
				{
					Node bNode = bnodeList.item(bcontNode);
					if(bNode.getFirstChild() != null)
						dataTransaction.put(bNode.getNodeName(), bNode.getFirstChild().getTextContent().trim());
				}
				
			}
			else
			{
				if(aNode.getFirstChild() != null)
					dataTransaction.put(aNode.getNodeName(), aNode.getFirstChild().getTextContent().trim());
			}		
		}
		
		return dataTransaction;
    }
    //==========================================================================================================================
	/**Se leen los nodos existentes pata el TAG que contiene los parametros de la transacción<p>
	 * 
	 * 
	 * @param data
	 * @param rootTagName
	 * @return <code> dataTransaction </code> <code>  </code>
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static HashMap<String, String> transformDocumentXMLToHashMapLoadAttributes(String data, String rootTagName) throws ParserConfigurationException, SAXException, IOException
    {
    	HashMap<String, String> dataTransaction = new HashMap<String, String>();
    	NamedNodeMap attrs;
    	DocumentBuilderFactory aDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder aDocumentBuilder;
		aDocumentBuilder = aDocumentBuilderFactory.newDocumentBuilder();
		Document doc = aDocumentBuilder.parse(new InputSource(new StringReader(data)));
		doc.getDocumentElement().normalize();
		
		// Se leen los nodos existentes pata el TAG que contiene los parametros de la transacción
		Element xmlRootElement = (Element)doc.getElementsByTagName(rootTagName).item(0);
		NodeList nodeList = xmlRootElement.getChildNodes();
		for (int contNode = 0; contNode < nodeList.getLength(); contNode++) 
		{
			Node aNode = nodeList.item(contNode);
			if(aNode.getFirstChild() != null)
			{	 
			attrs = (NamedNodeMap)aNode.getFirstChild().getAttributes();
			 for(int i = 0 ; i<attrs.getLength() ; i++) {
			        Attr attribute = (Attr)attrs.item(i);     
			        dataTransaction.put(attribute.getName(), attribute.getValue().trim());
			      }				
			}
		}		
		return dataTransaction;
    }
	//==========================================================================================================================
	/**
	 * 
	 * @param filePath
	 * @return <code>IOUtils.toString(new FileInputStream(filePath), Charsets.UTF_8)  </code> <code>  </code> <code>  </code>
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String loadFile(String filePath) throws FileNotFoundException, IOException
	{
		return IOUtils.toString(new FileInputStream(filePath), Charsets.UTF_8);
	}
	//==========================================================================================================================
	public String getMAC(String mensaje, String llavesMac, String firstHexaBlock)
	{
		try
		{
			if(mensaje.length()%8 > 0)
			{
				mensaje = Util.paddingText(mensaje.length()+8-mensaje.length()%8, " ", mensaje, false);
			}
			
			Matcher matcher	= Pattern.compile(".{1,8}").matcher(mensaje);
			byte[] total 	= hexStringToByteArray(firstHexaBlock);
			do
			{
				if(matcher.find())
				{
					byte[] tempArray = hexStringToByteArray(stringToHex(mensaje.substring(matcher.start(), matcher.end())));
					byte[] temp = new byte[tempArray.length];
					
					for(int cont = 0; cont < tempArray.length; cont++)
					{
						temp[cont] = (byte)(total[cont] ^ tempArray[cont]); 
					}
					
					total = hexStringToByteArray(TripleDES.encrypt(Util.bytesToHex(temp), llavesMac,"/CBC/NoPadding", true));
				}
			}while(!matcher.hitEnd());            
            return Util.bytesToHex(total).substring(0, 8) + "00000000";
		} 
		catch (Exception exception)
		{
			Util.log(exception.getMessage(), exception);
			return null;
		}
	}
	//==========================================================================================================================
	public static byte[] addPadding(byte[] input)
	{
		int extra = 8 - (input.length % 8);
        int newLength = input.length + extra;
        byte[] out = Arrays.copyOf(input, newLength);
        int offset = input.length;       
        while (offset < newLength)
        {
            out[offset] = (byte) 32;
            offset++;
        }
        return out;
	}
	//====================================================================================
    /**Toma la data enviada y la convierte en un String de caracteres, separando los objetos
     * por una barra vertical
     * 
     * @param data
     * @return infoEnviada
     */
    public String converInfoSent(HashMap<String, Object> data) 
    {
    	Iterator<Entry<String, Object>> dataIterator = data.entrySet().iterator();
    	
    	String infoEnviada = "";
    	while (dataIterator.hasNext()) 
    	{
    		Map.Entry<String, Object> dataEntry = (Map.Entry<String, Object>)dataIterator.next();
    		infoEnviada += (infoEnviada.equals("")?"":"|") + dataEntry.getKey() + "=" + dataEntry.getValue();
    	}
    	
    	return infoEnviada;
    }
    //====================================================================================
}