package com.bankVision.webBanking.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**se convierte la cadena de bytes en su representación hexadecimal<p>
 * Auto-generated catch block
 * 
 * @author Bankvision Software
 * @version 3.1.27
 *
 */
public class SHA1 
{
	// ====================================================================================
	/**se convierte la cadena de bytes en su representación hexadecimal<p>
	 * Auto-generated catch block
	 * 
	 * @param source
	 * @return
	 */
	public static String encode(String source) 
	{
		byte[] defaultBytes = source.getBytes();
		StringBuffer hexString = new StringBuffer();

		try {
			MessageDigest algorithm = MessageDigest.getInstance("SHA-1");
			algorithm.reset();
			algorithm.update(defaultBytes);
			byte messageDigest[] = algorithm.digest();
			// se convierte la cadena de bytes en su representación hexadecimal
			for (int i = 0; i < messageDigest.length; i++) 
			{
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}
		} catch (NoSuchAlgorithmException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hexString + "";
	}
	// ====================================================================================
}

