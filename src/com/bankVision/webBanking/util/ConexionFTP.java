package com.bankVision.webBanking.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.StringTokenizer;
/**Crea una conexion FTP con AS400
 * 
 * @author Bankvision Software
 * @version Realiza las conexiones a as400 por ftp
 *
 */
public class ConexionFTP 
{	
	private Socket socket = null;
	private BufferedReader reader = null;
	private BufferedWriter writer = null;
	private static boolean DEBUG = false;
	
    /**
     * Se conecta al as/400
     * @param host
     * @param puerto
     * @param usuario
     * @param password
     * @param respuesta
     * @throws IOException
     */
	public synchronized void conectar(String host, int puerto, String usuario, String password, String respuesta) throws IOException 
	{
		socket = new Socket(host, puerto);
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		respuesta = readLine();
		if (!respuesta.startsWith("220-")) 
		{
			throw new IOException(respuesta);
		}
		else
		{
			Util.log(respuesta);
		}
		respuesta = readLine();
		if (!respuesta.startsWith("220 ")) 
		{
			throw new IOException(respuesta);
		}
		else
		{
			Util.log(respuesta);
		}
		enviarLinea("USER " + usuario);
	
		respuesta = readLine();
		if (!respuesta.startsWith("331 ")) 
		{
			throw new IOException(respuesta);
		}
		else
		{
			Util.log(respuesta);
		}
		enviarLinea("PASS " + password);
	
		respuesta = readLine();
		if (!respuesta.startsWith("230 ")) 
		{
			throw new IOException(respuesta);
		}
		else
		{
			Util.log(respuesta);
		}
	    // Ahora esta conectado
	}

	/**
	 * Ubicar la bilioteca donde se va a copiar el archivo. Retorna true si es exitoso.
	 * @param biblioteca
	 * @return
	 * @throws IOException
	 */
	public synchronized boolean biblioteca(String biblioteca) throws IOException 
	{
		enviarLinea("CWD " + biblioteca);
		String respuesta = readLine();
		Util.log(respuesta);
		return (respuesta.startsWith("250 "));
	}
	
	/**
	 * Envía el archivo al as/400. Si la transferencia es exitosa devuelve true.
	 * @param Archivo
	 * @return
	 * @throws IOException
	 */
	public synchronized boolean enviarArchivo(File Archivo) throws IOException 
	  {
		  if (Archivo.isDirectory()) 
		  {
			  throw new IOException("No se pudo cargar el archivo");
		  }
		  String nombreArchivo = Archivo.getName();
		  int bytesLeidos = (int) Archivo.length();
		  Util.log(bytesLeidos + " bytes leidos en el archivo " + nombreArchivo);
		  return enviarArchivo(new FileInputStream(Archivo), nombreArchivo);
	  }
	
	/**
	 * Modo pasivo de envio al as/400
	 * @param inputStream
	 * @param nombreArchivo
	 * @return
	 * @throws IOException
	 */
	public synchronized boolean enviarArchivo(InputStream inputStream, String nombreArchivo) throws IOException 
	  {
		  BufferedInputStream input = new BufferedInputStream(inputStream);
		  enviarLinea("PASV");
		  String respuesta = readLine();
		  if (!respuesta.startsWith("227 ")) 
		  {
			  throw new IOException(respuesta);
		  }
		  else
		  {
			  Util.log(respuesta);
		  }
		  String host = null;
		  int puerto = -1;
		  int abierto = respuesta.indexOf('(');
		  int cerrado = respuesta.indexOf(')', abierto + 1);
		  if (cerrado > 0) 
		  {
			  String direccion = respuesta.substring(abierto + 1, cerrado);
			  StringTokenizer tokenizer = new StringTokenizer(direccion, ",");
			  try 
			  {
				  host = tokenizer.nextToken() + "." + tokenizer.nextToken() + "." + tokenizer.nextToken() + "." + tokenizer.nextToken();
				  puerto = Integer.parseInt(tokenizer.nextToken()) * 256 + Integer.parseInt(tokenizer.nextToken());
			  } 
			  catch (Exception e) 
			  {
				  throw new IOException("Se recibieron mal los datos de transmisión: " + respuesta);
			  }
		  }
		  enviarLinea("STOR " + nombreArchivo);
		  Socket dataSocket = new Socket(host, puerto);
		  respuesta = readLine();
		  if (!respuesta.startsWith ("150 ")) 
		  {
			  dataSocket.close();
			  throw new IOException("No se permite enviar el archivo: " + respuesta);
		  }
		  else
		  {
			  Util.log(respuesta);
		  }
		  BufferedOutputStream output = new BufferedOutputStream(dataSocket.getOutputStream());
		  byte[] buffer = new byte[4096];
		  int bytesRead = 0;
		  while ((bytesRead = input.read(buffer)) != -1) 
		  {
			  output.write(buffer, 0, bytesRead);
		  }
		  output.flush();
		  output.close();
		  input.close();
		  respuesta = readLine();
		  if (!respuesta.startsWith("226 ")) 
		  {
			  dataSocket.close();
			  throw new IOException(respuesta);
		  }
		  else
		  {
			  Util.log(respuesta);
			  dataSocket.close();
			  return respuesta.startsWith("226 ");
		  }
	  }
	
	/**
	 * Enviar mensajes al as/400
	 * @param linea
	 * @throws IOException
	 */
	private synchronized void enviarLinea(String linea) throws IOException 
	  {
		  if (socket == null) 
		  {
			  throw new IOException("No esta conectado");
		  }
		  try 
		  {
			  writer.write(linea + "\r\n");
			  writer.flush();
			  if (DEBUG) 
			  {
				  Util.log("> " + linea);
			  }
		  } 
		  catch (IOException e) 
		  {
			  socket = null;
			  throw e;
		  }
	  }
	
	/**
	 * Leer las respuestas del as/400
	 * @return
	 * @throws IOException
	 */
	private String readLine() throws IOException 
	  {
		  String linea = reader.readLine();
		  if (DEBUG) 
		  {
			  Util.log("< " + linea);
		  }
		  return linea;
	  }
	  
	/**
	 *  Desconectar del as/400
	 * @throws IOException
	 */
	public void desconectar() throws IOException 
		{
			enviarLinea("QUIT");
			String respuesta = readLine();
			Util.log(respuesta);
		}
}