package com.bankVision.webBanking.util;

import java.io.UnsupportedEncodingException;
/**Realiza la codificacion y decodificacion en base64
 * 
 * @author Bankvision Software
 * @version 3.1.27
 *
 */
public class Base64 
{
	public static String base64code = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
									+ "abcdefghijklmnopqrstuvwxyz" + "0123456789" + "+/";
	public static int splitLinesAt = 76;
	
	/**
     * Marker value for chars we just ignore, e.g. \n \r high ascii.
     *
     * @noinspection WeakerAccess
     */
    protected static final int IGNORE = -1;
    
    /**
     * Marker for = trailing pad.
     *
     * @noinspection WeakerAccess
     */
    protected static final int PAD = -2;
    
    /**
     * special character 1, will be - in Base64u.
     *
     * @noinspection WeakerAccess
     */
    protected static char spec1 = '+';

    /**
     * special character 2, will be _ in Base64u.
     *
     * @noinspection WeakerAccess
     */
    protected static char spec2 = '/';
    
    /**
     * special character 3, will be * in Base64u.
     *
     * @noinspection WeakerAccess
     */
    protected static char spec3 = '=';

    /**Contiene el algoritmo para la codificacion
     * 
     * @param string
     * @return
     */
	public static String base64Encode(String string) 
	{
		String encoded = "";
		// determine how many padding bytes to add to the output
		int paddingCount = (3 - (string.length() % 3)) % 3;
		// add any necessary padding to the input
		string += "\0\0".substring(0, paddingCount);
		// process 3 bytes at a time, churning out 4 output bytes
		// worry about CRLF insertions later
		for (int i = 0; i < string.length(); i += 3) 
		{
			int j = (string.charAt(i) << 16) + (string.charAt(i + 1) << 8)
					+ string.charAt(i + 2);
			encoded = encoded + base64code.charAt((j >> 18) & 0x3f)
					+ base64code.charAt((j >> 12) & 0x3f)
					+ base64code.charAt((j >> 6) & 0x3f)
					+ base64code.charAt(j & 0x3f);
		}
		// replace encoded padding nulls with "="
		return splitLines(encoded.substring(0, encoded.length() - paddingCount) + "==".substring(0, paddingCount));
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public static String splitLines(String string) 
	{
		String lines = string.substring(0, Math.min(string.length(), splitLinesAt));
		for (int i = Math.min(string.length(), splitLinesAt); i < string.length(); i += splitLinesAt) 
		{
			lines += "\r\n";
			lines += string.substring(i, Math.min(string.length(), i + splitLinesAt));
		}
		return lines;
	}
	
	private static final char[] map1 = new char[64];
	static 
	{
		// build translate valueToChar table only once.
        // 0..25 -> 'A'..'Z'
		for ( int i = 0; i <= 25; i++ ) map1[ i ] = ( char ) ( 'A' + i );
		// 26..51 -> 'a'..'z'
		for ( int i = 0; i <= 25; i++ ) map1[ i + 26 ] = ( char ) ( 'a' + i );
		// 52..61 -> '0'..'9'
		for ( int i = 0; i <= 9; i++ ) 	map1[ i + 52 ] = ( char ) ( '0' + i );
		
		map1[ 62 ] = spec1;
		map1[ 63 ] = spec2; 
	}

	// Mapping table from Base64 characters to 6-bit nibbles.
	private static final int[] map2 = new int[256];
	static 
	{
		// build translate charToValue table only once.
        for ( int i = 0; i < 256; i++ ) map2[ i ] = IGNORE;// default is to ignore
        for ( int i = 0; i < 64; i++ )	map2[ map1[ i ] ] = i;
        map2[spec3] = PAD; 
	}

	/**Lanza la decodificacion
	 * 
	 * @param s
	 * @return
	 */
	public static String decodeString(String s)
	{
		String decodeString = null;
		try 
		{
			decodeString = new String(decodeBase64U(s), "8859_1"/* encoding */);
		} 
		catch (UnsupportedEncodingException e) 
		{
			Util.log(e.getLocalizedMessage(), e);
		}
		return decodeString;
	}
	
	/**
     * decode a well-formed complete Base64 string back into an array of bytes. It must have an even multiple of 4 data
     * characters (not counting \n), padded out with = as needed.
     *
     * @param s base64-encoded string
     *
     * @return plaintext as a byte array.
     * @noinspection WeakerAccess, CanBeFinal
     */
    public static byte[] decodeBase64U(String s)
    {
        // estimate worst case size of output array, no embedded newlines.
        byte[] b = new byte[ ( s.length() / 4 ) * 3 ];
        // tracks where we are in a cycle of 4 input chars.
        int cycle = 0;
        // where we combine 4 groups of 6 bits and take apart as 3 groups of 8.
        int combined = 0;
        // how many bytes we have prepared.
        int j = 0;
        // will be an even multiple of 4 chars, plus some embedded \n
        int len = s.length();
        int dummies = 0;
        for ( int i = 0; i < len; i++ )
        {
            int c = s.charAt( i );
            int value = ( c <= 255 ) ? map2[ c ] : IGNORE;
            // there are two magic values PAD (=) and IGNORE.
            switch ( value )
            {
                case IGNORE:
                    // e.g. \n, just ignore it.
                    break;
                case PAD:
                    value = 0;
                    dummies++;
                    // deliberate fallthrough
                default:
                    /* regular value character */
                    switch ( cycle )
                    {
                        case 0:
                            combined = value;
                            cycle = 1;
                            break;
                        case 1:
                            combined <<= 6;
                            combined |= value;
                            cycle = 2;
                            break;
                        case 2:
                            combined <<= 6;
                            combined |= value;
                            cycle = 3;
                            break;
                        case 3:
                            combined <<= 6;
                            combined |= value;
                            // we have just completed a cycle of 4 chars.
                            // the four 6-bit values are in combined in
                            // big-endian order
                            // peel them off 8 bits at a time working lsb to msb
                            // to get our original 3 8-bit bytes back
                            b[ j + 2 ] = ( byte ) combined;
                            combined >>>= 8;
                            b[ j + 1 ] = ( byte ) combined;
                            combined >>>= 8;
                            b[ j ] = ( byte ) combined;
                            j += 3;
                            cycle = 0;
                            break;
                    }
                    break;
            }
        }// end for
        if ( cycle != 0 )
        {
            throw new ArrayIndexOutOfBoundsException("Input to decode not an even multiple of 4 characters; pad with " + spec3);
        }
        j -= dummies;
        if ( b.length != j )
        {
            byte[] b2 = new byte[ j ];
            System.arraycopy( b, 0, b2, 0, j );
            b = b2;
        }
        return b;
    }// end decode
}