package com.bankVision.webBanking.util;

import java.math.BigDecimal;
/**Realiza todas las conversiones de los valores numericos ingresado a valores en letras
 * 
 * 
 * @author Bankvision Software
 * @version 3.1.27
 *
 */
public class ConvertidorNumerosAPalabras 
{
	private int flag;
	public long numero;
	
	
	long MIL  			= 1000;
	long MILLON  		= 1000000;
	long MILMILLONES  	= MIL * MILLON;
	long BILLON  		= MIL * MILMILLONES;
	long MILBILLONES  	= MIL * BILLON;
	
	
//========================================================================================================================	
	/**
	 * Setea la variable con valo 0
	 */
	public ConvertidorNumerosAPalabras()
	{
		numero = 0;
		flag=0;
	}
//========================================================================================================================	
	/**Setea la variable con el valor del parametro n que recibe
	 * 
	 * @param n
	 */
	public ConvertidorNumerosAPalabras(long n)
	{
		numero = n;
		flag=0;
	}
//========================================================================================================================	
	/**Metoco que recibe la unidad y retorna su valor en letras
	 * 
	 * @param numero
	 * @return
	 */
	private String unidad(int numero)
	{
		String numLetras = "";
		switch (numero)
		{
			case 9:	numLetras = "nueve";	break;
			case 8:	numLetras = "ocho";	break;
			case 7:	numLetras = "siete";	break;
			case 6:	numLetras = "seis";	break;
			case 5:	numLetras = "cinco";	break;
			case 4:	numLetras = "cuatro";	break;
			case 3:	numLetras = "tres";	break;
			case 2:	numLetras = "dos";	break;
			case 1:	numLetras = (flag == 0)?"uno":"un";	break;
			case 0:	numLetras = "";		break;
		}
		return numLetras;
	}
//========================================================================================================================	
	/**Convierte las decenas a su valor numerico en letras
	 *  
	 * @param numero
	 * @return
	 */
	private String decena(int numero)
	{
		String numLetras = "";
		if (numero >= 90 && numero <= 99)
		{
			numLetras = "noventa ";
			if (numero > 90)
				numLetras = numLetras.concat("y ").concat(unidad(numero - 90));
		}
		else if (numero >= 80 && numero <= 89)
		{
			numLetras = "ochenta ";
			if (numero > 80)
				numLetras = numLetras.concat("y ").concat(unidad(numero - 80));
		}
		else if (numero >= 70 && numero <= 79)
		{
			numLetras = "setenta ";
			if (numero > 70)
				numLetras = numLetras.concat("y ").concat(unidad(numero - 70));
		}
		else if (numero >= 60 && numero <= 69)
		{
			numLetras = "sesenta ";
			if (numero > 60)
				numLetras = numLetras.concat("y ").concat(unidad(numero - 60));
		}
		else if (numero >= 50 && numero <= 59)
		{
			numLetras = "cincuenta ";
			if (numero > 50)
				numLetras = numLetras.concat("y ").concat(unidad(numero - 50));
		}
		else if (numero >= 40 && numero <= 49)
		{
			numLetras = "cuarenta ";
			if (numero > 40)
				numLetras = numLetras.concat("y ").concat(unidad(numero - 40));
		}
		else if (numero >= 30 && numero <= 39)
		{
			numLetras = "treinta ";
			if (numero > 30)
				numLetras = numLetras.concat("y ").concat(unidad(numero - 30));
		}
		else if (numero >= 20 && numero <= 29)
		{
			if (numero == 20)
				numLetras = "veinte ";
			else
				numLetras = "veinti".concat(unidad(numero - 20));
		}
		else if (numero >= 10 && numero <= 19)
		{
			switch (numero)
			{
				case 10: numLetras = "diez ";		break;
				case 11: numLetras = "once ";		break;
				case 12: numLetras = "doce ";		break;
				case 13: numLetras = "trece ";		break;
				case 14: numLetras = "catorce ";	break;
				case 15: numLetras = "quince ";		break;
				case 16: numLetras = "dieciseis ";	break;
				case 17: numLetras = "diecisiete ";	break;
				case 18: numLetras = "dieciocho ";	break;
				case 19: numLetras = "diecinueve ";	break;
			}	
		}
		else
			numLetras = unidad(numero);
		return numLetras;
	}	
//========================================================================================================================	
	/**Convierte la centena a su valor en letras
	 * 
	 * @param numero
	 * @return
	 */
	private String centena(int numero)
	{
		String numLetras = "";
		if (numero >= 100)
		{
			if (numero >= 900 && numero <= 999)
			{
				numLetras = "novecientos ";
				if (numero > 900)
					numLetras = numLetras.concat(decena(numero - 900));
			}
			else if (numero >= 800 && numero <= 899)
			{
				numLetras = "ochocientos ";
				if (numero > 800)
					numLetras = numLetras.concat(decena(numero - 800));
			}
			else if (numero >= 700 && numero <= 799)
			{
				numLetras = "setecientos ";
				if (numero > 700)
					numLetras = numLetras.concat(decena(numero - 700));
			}
			else if (numero >= 600 && numero <= 699)
			{
				numLetras = "seiscientos ";
				if (numero > 600)
					numLetras = numLetras.concat(decena(numero - 600));
			}
			else if (numero >= 500 && numero <= 599)
			{
				numLetras = "quinientos ";
				if (numero > 500)
					numLetras = numLetras.concat(decena(numero - 500));
			}
			else if (numero >= 400 && numero <= 499)
			{
				numLetras = "cuatrocientos ";
				if (numero > 400)
					numLetras = numLetras.concat(decena(numero - 400));
			}
			else if (numero >= 300 && numero <= 399)
			{
				numLetras = "trescientos ";
				if (numero > 300)
					numLetras = numLetras.concat(decena(numero - 300));
			}
			else if (numero >= 200 && numero <= 299)
			{
				numLetras = "doscientos ";
				if (numero > 200)
					numLetras = numLetras.concat(decena(numero - 200));
			}
			else if (numero >= 100 && numero <= 199)
			{
				if (numero == 100)
					numLetras = "cien ";
				else
					numLetras = "ciento ".concat(decena(numero - 100));
			}
		}
		else
			numLetras = decena(numero);
		
		return numLetras;	
	}	
//========================================================================================================================	
	/**Retorna el valor en letras de las unidades de miles
	 * 
	 * @param numero
	 * @return
	 */
	private String miles(int numero)
	{
		String numLetras = "";
		if (numero >= 1000 && numero <2000)
		{
			numLetras = ("mil ").concat(centena(numero%1000));
		}
		if (numero >= 2000 && numero <10000)
		{
			flag=1;
			numLetras = unidad(numero/1000).concat(" mil ").concat(centena(numero%1000));
		}
		if (numero < 1000)
			numLetras = centena(numero);
		
		return numLetras;
	}		
//========================================================================================================================	
	/**Retorna el valor en letras de las decenas de miles
	 * 
	 * @param numero
	 * @return
	 */
	private String decmiles(int numero)
	{
		String numLetras = "";
		if (numero == 10000)
			numLetras = "diez mil";
		if (numero > 10000 && numero <20000)
		{
			flag=1;
			numLetras = decena(numero/1000).concat("mil ").concat(centena(numero%1000));		
		}
		if (numero >= 20000 && numero <100000)
		{
			flag=1;
			numLetras = decena(numero/1000).concat(" mil ").concat(miles(numero%1000));		
		}
		if (numero < 10000)
			numLetras = miles(numero);
		
		return numLetras;
	}		
//========================================================================================================================	
	/**Retorna centenas de miles
	 * 
	 * @param numero
	 * @return
	 */
	private String cienmiles(int numero)
	{
		String numLetras = "";
		if (numero == 100000)
			numLetras = "cien mil";
		if (numero >= 100000 && numero <1000000)
		{
			flag=1;
			numLetras = centena(numero/1000).concat(" mil ").concat(centena(numero%1000));		
		}
		if (numero < 100000)
			numLetras = decmiles(numero);
		return numLetras;
	}		
//========================================================================================================================	
	/**Retorna millones
	 * 
	 * @param numero
	 * @return
	 */
	private String millon(int numero)
	{
		String numLetras = "";
		if (numero >= 1000000 && numero <2000000)
		{
			flag=1;
			numLetras = ("Un millon ").concat(cienmiles(numero%1000000));
		}
		if (numero >= 2000000 && numero <10000000)
		{
			flag=1;
			numLetras = unidad(numero/1000000).concat(" millones ").concat(cienmiles(numero%1000000));
		}
		if (numero < 1000000)
			numLetras = cienmiles(numero);
		
		return numLetras;
	}		
//========================================================================================================================	
	/**Retorna decenas de millones
	 * 
	 * @param numero
	 * @return
	 */
	private String decmillon(long numero)
	{
		String numLetras = "";
		if (numero == 10000000)
			numLetras = "diez millones";
		if (numero > 10000000 && numero <100000000)
		{
			flag=1;
			numLetras = decena((int)numero/1000000).concat(" millones ").concat(millon((int)numero%1000000));		
		}
		if (numero < 10000000)
			numLetras = millon((int)numero);
		
		return numLetras;
	}		
//========================================================================================================================
	/**Retorna centenas de millones
	 * 
	 * @param numero
	 * @return
	 */
	private String centenasMillon(long numero)
	{
		String numLetras = "";
		if (numero >= 100000000 && numero <1000000000)
		{
			flag=1;
			numLetras = centena((int)numero/1000000).concat(" millones ").concat(millon((int)numero%1000000));		
		}
		if (numero < 100000000)
			numLetras = decmillon(numero);
		return numLetras;
	}		
//========================================================================================================================	
	/**Retorna miles de millones
	 * 
	 * @param numero
	 * @return
	 */
	private String milesMillon(long numero)
	{
		String numLetras = "";
		if (numero >= MIL*MILLON  && numero <10*MIL*MILLON)
		{
			flag=1;
			numLetras = miles((int)(numero/MILLON)).concat(" millones ").concat(millon((int)(numero%MILLON)));		
		}
		if (numero < MIL*MILLON)
			numLetras = centenasMillon(numero);
		return numLetras;
	}		
//========================================================================================================================	
	/**Retorna decenas de miles de millones
	 * 
	 * @param numero
	 * @return
	 */
	private String decenasMilesMillon(long numero)
	{
		String numLetras = "";
		if (numero >= 10*MIL*MILLON  && numero <100*MIL*MILLON)
		{
			flag=1;
			numLetras =  decmiles((int)(numero/(MILLON))).concat(" millones ").concat(millon((int)(numero%MILLON)));		
		}
		if (numero < 10*MIL*MILLON)
			numLetras = milesMillon(numero);
		return numLetras;
	}		
//========================================================================================================================	
	/**Retorna centenas de miles de millones
	 * 
	 * @param numero
	 * @return
	 */
	private String centenasMilesMillon(long numero)
	{
		String numLetras = "";
		if (numero >= 100*MIL*MILLON  && numero < BILLON)
		{
			flag=1;
			numLetras =  cienmiles((int)(numero/(MILLON))).concat(" millones ").concat(millon((int)(numero%MILLON)));		
		}
		if (numero < 100*MIL*MILLON)
			numLetras = decenasMilesMillon(numero);
		return numLetras;
	}		
//========================================================================================================================	
	/**Retorna billones
	 * 
	 * @param numero
	 * @return
	 */
	private String billones(long numero)
	{
		String numLetras = "";
		if (numero >= BILLON && numero <2*BILLON)
		{
			flag=1;
			numLetras = ("Un billon ").concat(centenasMilesMillon(numero%1000000));
		}
		if (numero >= 2*BILLON && numero <10*BILLON)
		{
			flag=1;
			numLetras = unidad((int)(numero/BILLON)).concat(" billones ").concat(centenasMilesMillon(numero%BILLON));
		}
		if (numero < BILLON)
			numLetras = centenasMilesMillon(numero);
		
		return numLetras;
	}		
//========================================================================================================================		
	/**se llama la función billones, la cual inicia la conversión llamando a las demas (centenasMilesMillon, cienMiles .. .etc)<p>
	 * si el número es en el orden de Billones o Millones enteros se agrega la palabra 'de'
	 * 
	 * @param numero
	 * @return
	 */
	public String convertirMoneda(BigDecimal numero)
	{
		// se llama la función billones, la cual inicia la conversión llamando a las demas (centenasMilesMillon, cienMiles .. .etc)
		String numLetras = billones(numero.longValue());
		// si el número es en el orden de Billones o Millones enteros se agrega la palabra 'de'
		if ((numero.longValue() >= BILLON && numero.longValue()%BILLON == 0)
			||(numero.longValue() >= MILLON && numero.longValue()%MILLON == 0))
			numLetras += " de";

		numLetras += " pesos";
			
		if (numero.doubleValue() - numero.longValue() != 0) 
			numLetras = numLetras + " con " + centena((int)((numero.movePointRight(2).longValue() - numero.longValue()*100))) + " centavos.";
		return numLetras;
	}
//========================================================================================================================
	/**Retorna el valor en letras del numero recibido
	 * 
	 * @param numero
	 * @return
	 */
	public String convertirNumero(BigDecimal numero)
	{
		String numLetras = billones(numero.longValue());
		return numLetras;
	} 	
//========================================================================================================================
	/**Retorna el valor en letras de el valor con sus decimales
	 * 
	 * @param numero
	 * @param numeroDecimales
	 * @return
	 */
	public String convertirTasa(BigDecimal numero, int numeroDecimales)
	{
		BigDecimal tasa = numero.movePointLeft(-2+numeroDecimales); 
		String numLetras = tasa.longValue() == 0?"cero":billones(tasa.longValue());
		if (tasa.toString().split("\\.").length > 1)
		{
			String parteDecimal = tasa.toString().split("\\.")[1];
			numLetras = numLetras + " punto";
        	for (int cont = 0;cont < parteDecimal.length();cont++)
        	{
        		int numeroDecimal = Integer.parseInt(Character.toString(parteDecimal.charAt(cont)));
        		numLetras = numLetras + " " + (numeroDecimal == 0?"cero":unidad(numeroDecimal));
        	}
		}
		return numLetras;
	}
}
